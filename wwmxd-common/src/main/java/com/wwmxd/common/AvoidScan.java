package com.wwmxd.common;

import java.lang.annotation.*;

/**
 * 忽略扫描
 * @author lw
 * @date 2018-10-24
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AvoidScan {

}
