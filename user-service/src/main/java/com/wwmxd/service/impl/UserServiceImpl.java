package com.wwmxd.service.impl;

import com.wwmxd.entity.User;
import com.wwmxd.dao.UserDao;
import com.wwmxd.service.UserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService  {
	
}
